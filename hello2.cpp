///////////////////////////////////////////////////////////////////////////    /////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - HELLO C++
//
// Usage:  hello2.cpp
//
// Result:
//   Hello World!
//
// @author eduardo kho jr <eduardok@hawaii.edu>
// @date   2/10/2021
///////////////////////////////////////////////////////////////////////////    ////

#include<iostream>

int main(){

std::cout << "Hello World!" << std::endl;

}











