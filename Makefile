#########################################################################################
## University of Hawaii, College of Engineering
## EE 205 - Object Oriented Programming
## Lab 04c - HELLO C++
##
## Usage:  hello1.cpp
##
## Result:
##   Hello World!
##
## @author eduardo kho jr <eduardok@hawaii.edu>
## @date   2/10/2021
#########################################################################################

TARGET: hello1 hello2

make: $(TARGET)

hello1: hello1.cpp
	g++ -o hello1 hello1.cpp
hello2: hello2.cpp
	g++ -o hello2 hello2.cpp
clean:
	rm -f $(TARGET) 








